# Hello, guys!

I'm a programming student who wants to learn more about web development, game development and cybersecurity

### Areas

- Web development 🕸️
- Game development 🎮
- Studying cybersecurity 🔒

### Portfolio (in development)
- <https://arthur-devs-portfolio.vercel.app/>

![pfp](https://avatars.githubusercontent.com/u/73297053?v=4)

<div>
  <a href="https://github.com/arthurdeveloper">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=arthurdeveloper&show_icons=true&theme=tokyonight&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=arthurdeveloper&layout=compact&langs_count=7&theme=tokyonight"/>
</div>
  
![Snake animation](https://github.com/ArthurDeveloper/ArthurDeveloper/blob/output/github-contribution-grid-snake.svg)

```js
const skills = {
  'techs': [
    'HTML',
    'CSS',
    'JS',
    'Python',
    'React',
    'Next',
    'Node',
    'Sass',
    'PHP'
  ],
  
  'databases': [
    'MySQL',
    'Firebase'
  ],
  
  'other': [
    'Git/Github',
    'UI/UX'
  ]
}
```
